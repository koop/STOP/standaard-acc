﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Schema.Server.Controllers
{
	[Controller]
	public class SchemaController : ControllerBase
	{
		#region Services
		[HttpGet]
		[Route ("ping")]
		public ActionResult Ping ()
		{
			_Logger?.LogInformation ($"Ping");
			return Content ("Ping");
		}

		[HttpGet]
		[Route ("get/{project}/{file}")]
		public ActionResult Get (string project, string file, string version)
		{
			var url = GetUrl (project, file, version);
			if (url != null)
			{
				try
				{
					using var wc = new WebClient ();
					return Content (wc.DownloadString (url));
				}
				catch (Exception ex)
				{
					_Logger?.LogInformation ($"Cannot download '{url}': {ex}");
				}
			}
			return NotFound ();
		}

		[HttpGet]
		[Route ("goto/{project}/{file}")]
		public ActionResult Goto (string project, string file, string version)
		{
			var url = GetUrl (project, file, version);
			if (url != null)
			{
				return Redirect (url);
			}
			return NotFound ();
		}
		#endregion

		#region Construction
		public SchemaController (ILogger<SchemaController> logger)
		{
			_Logger = logger;
		}
		private readonly ILogger<SchemaController> _Logger;
		#endregion

		#region Read configuration
		private string GetUrl (string project, string file, string version)
		{
			_Logger?.LogInformation ($"Project: '{project}', File: '{file}', Version: '{version}'");
			Data data = null;
			try
			{
				using var wc = new WebClient ();
				using var buffer = new MemoryStream (wc.DownloadData ("http://koop.gitlab.io/STOP/standaarden.overheid.nl/Schema.Server.json"));
				data = _Serializer.ReadObject (buffer) as Data;
			}
			catch (Exception ex)
			{
				_Logger?.LogInformation ($"Cannot download/parse configuration: {ex}");
				return null;
			}
			if (data != null)
			{
				foreach (var item in data)
				{
					if (item.Project == project && item.Version == version)
					{
						var result = item.Url;
						if (!result.EndsWith ("/"))
						{
							result += "/";
						}
						result += file;
						_Logger?.LogInformation ($"Schema url: '{result}'");
						return result;
					}
				}
			}
			_Logger.LogInformation ("Project/version not found");
			return null;
		}
		private static readonly DataContractJsonSerializer _Serializer = new DataContractJsonSerializer (typeof (Data));

		[CollectionDataContract]
		private class Data : List<ProjectVersion>
		{
		}

		[DataContract]
		private class ProjectVersion
		{
			[DataMember]
			public string Project = null;
			[DataMember]
			public string Version = null;
			[DataMember]
			public string Url = null;
		}
		#endregion
	}
}
